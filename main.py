import errno
import time

import bluetooth
import usocket as socket
from machine import Pin, Timer
from micropython import const

from ota import check_for_update
from storage import config
from tracker import Tracker
from web import handle_request
from door import Door

_IRQ_SCAN_RESULT = const(5)
_IRQ_SCAN_DONE = const(6)
b1b = Pin(26, mode=Pin.OUT, value=0, pull=Pin.PULL_DOWN)
b1a = Pin(27, mode=Pin.OUT, value=0, pull=Pin.PULL_DOWN)
led = Pin(2, Pin.OUT)
tim0 = Timer(0)
tim1 = Timer(1)

door = Door(27, 26, 10)
door.close()


def bt_irq(event, data):
    if event == _IRQ_SCAN_RESULT:
        # A single scan result.
        addr_type, addr, connectable, rssi, adv_data = data
        _addr = ":".join(["%02X" % i for i in addr])

        track = Tracker.tracking.get(_addr)

        # decode_adv(adv_data)

        if track:
            print(_addr, rssi)
            led.on()
            track.track(rssi)
    elif event == _IRQ_SCAN_DONE:
        # Scan duration finished or manually stopped.
        # print('scan complete')
        pass


def loop(_):
    for t in Tracker.tracking.values():
        if (time.time() - t.last_seen) > 15:
            door.close()
        else:
            door.open()


def create_cb(bt):
    def cb(t):
        try:
            bt.gap_scan(3000, 30000, 30000)
            led.off()
        except OSError as e:
            if e.errno == errno.EALREADY:
                pass
            else:
                raise

    return cb


def network_status():
    import network

    wifi = network.WLAN(network.STA_IF)
    ap = network.WLAN(network.AP_IF)
    return {
        "wifi": wifi.active(),
        "ap": ap.active(),
    }


def main():
    net = network_status()
    print(net)
    if not net["wifi"]:
        led.on()
        time.sleep(0.5)
        led.off()
        print("Start repl")
        return
    else:
        try:
            check_for_update()
        except Exception as e:
            print(f"Failed to check for update:", e)

    Tracker.init(config.get("trackers", []))
    # DD:34:02:08:FB:C7

    bt = bluetooth.BLE()
    bt.active(True)
    bt.irq(bt_irq)

    tim0.init(period=1000, mode=Timer.PERIODIC, callback=loop)
    tim1.init(
        period=3500,
        mode=Timer.PERIODIC,
        callback=create_cb(bt),
    )

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(("", 80))
    server_socket.listen(3)
    print("Server started")
    try:
        while True:
            client_socket, addr = server_socket.accept()
            print("Connection from:", addr)
            try:
                handle_request(client_socket)
            except OSError as e:
                client_socket.close()
                continue
    finally:
        tim0.deinit()
        tim1.deinit()


main()
