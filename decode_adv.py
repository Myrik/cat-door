import struct


def read_adv(adv: bytes):
    _adv_data = adv
    while _adv_data:
        l, t = struct.unpack(">BB", _adv_data[:2])
        payload = _adv_data[2 : 1 + l]
        _adv_data = _adv_data[1 + l :]
        yield t, payload


def decode_adv(adv_data):
    for t, i in read_adv(adv_data):
        if t == 0xFF and len(i) == 25:
            manufacturer_data, uuid, major, minor, tx_power = struct.unpack(
                ">4s16sHHb", i
            )
            return manufacturer_data, uuid, major, minor, tx_power
