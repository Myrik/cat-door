import json

import requests
import os

base = "https://gitlab.com/Myrik/cat-door/-/raw/main"


def update_file(file):
    try:
        data = requests.get(f"{base}/{file}").content
    except Exception as e:
        print(e)
        return
    with open(file, "wb") as f:
        f.write(data)


def check_for_update():
    new_files = requests.get(f"{base}/sync.json").json()
    with open("sync.json", "r") as f:
        curr_files = json.loads(f.read())

    for file in set(new_files.keys()) | set(curr_files.keys()):
        if curr_files.get(file) != new_files.get(file):
            # no file, download
            if not curr_files.get(file):
                print(f"Downloading {file}")
                update_file(file)
            # delete file
            elif not new_files.get(file):
                print(f"Deleting {file}")
                os.remove(file)
            # update file
            else:
                print(f"Updating {file}")
                update_file(file)
