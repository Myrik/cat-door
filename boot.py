# This file is executed on every boot (including wake-boot from deepsleep)
# import esp
# esp.osdebug(None)

import network
import ntptime
import json
from time import sleep

import time
from machine import Pin

conn_status = {
    network.STAT_CONNECTING: "STAT_CONNECTING",
    network.STAT_WRONG_PASSWORD: "STAT_WRONG_PASSWORD",
    network.STAT_NO_AP_FOUND: "STAT_NO_AP_FOUND",
    network.STAT_ASSOC_FAIL: "STAT_ASSOC_FAIL",
    network.STAT_BEACON_TIMEOUT: "STAT_BEACON_TIMEOUT",
    network.STAT_HANDSHAKE_TIMEOUT: "STAT_HANDSHAKE_TIMEOUT",
    network.STAT_GOT_IP: "STAT_GOT_IP",
    network.STAT_IDLE: "STAT_IDLE",
}

def do_connect(ssid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print("Connecting to network...")
        wlan.connect(ssid, password)
        first = True
        while 1:
            status = wlan.status()

            if status == network.STAT_CONNECTING:
                sleep(1)
                continue

            if (
                status
                in (
                    network.STAT_ASSOC_FAIL,
                    network.STAT_BEACON_TIMEOUT,
                    network.STAT_HANDSHAKE_TIMEOUT,
                )
                and first
            ):
                print(f"Failed to connect to network, reconnect")
                first = False
                sleep(1)
                wlan.connect(ssid, password)
                continue

            if status in (
                network.STAT_WRONG_PASSWORD,
                network.STAT_NO_AP_FOUND,
                network.STAT_ASSOC_FAIL,
                network.STAT_BEACON_TIMEOUT,
                network.STAT_HANDSHAKE_TIMEOUT,
            ):
                print(f"Failed to connect to network: {conn_status.get(status, status)}")
                wlan.active(False)
                break

            if status == network.STAT_GOT_IP:
                print(f"Connected")
                break

            print(f"Connecting status: {conn_status.get(status, status)}")
            break

        ntptime.settime()

    return wlan


def do_ap(ssid, password):
    wlan = network.WLAN(network.AP_IF)
    wlan.active(True)
    wlan.config(essid=ssid, password=password, authmode=3)

    return wlan


def start_fallback_ap():
    import webrepl
    print("Starting fallback AP")
    wifi = do_ap("FallbackAP", "91234567")
    webrepl.start()
    return wifi


def main():
    with open("wifi.json", "r") as f:
        settings = json.loads(f.read())

    try:
        wlan = do_connect(**settings)
    except Exception as e:
        print("Failed to connect:", e)
        wlan = start_fallback_ap()
    else:
        if not wlan.isconnected():
            wlan = start_fallback_ap()

    print("network config:", wlan.ifconfig())


main()
