#!/usr/bin/env python3
import json

from mpremote.commands import show_progress_bar
from mpremote.main import State


import sys
import hashlib

BUF_SIZE = 65536


def upload(files):
    state = State()
    state.ensure_raw_repl()
    state.did_action()
    state.transport.filesystem_command(
        ["cp"] + list(files) + [":"], progress_callback=show_progress_bar, verbose=True
    )


def file_hash(file):
    md5 = hashlib.md5()

    with open(file, "rb") as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            md5.update(data)

    return md5.hexdigest()


def main():
    with open("sync.json", "r") as f:
        files = json.loads(f.read())

    changes = False
    for file, h in files.items():
        curr_hash = file_hash(file)
        if curr_hash != h:
            if not changes and file == "sync.json":
                continue
            changes = True
            print(f"Hash changed for {file}: {h} -> {curr_hash}")
            files[file] = curr_hash

    if changes:
        with open("sync.json", "w") as f:
            f.write(json.dumps(files, indent=True))

    # upload(files.keys())


if __name__ == "__main__":
    main()
