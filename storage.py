import json


class Storage:
    def __init__(self, name):
        self.name = name + ".json"
        self.data = {}
        self.read()

    def read(self):
        try:
            with open(self.name, "r") as f:
                self.data = json.loads(f.read())
        except Exception as e:
            print(f"Error reading confir {self.name!r}: {e}")

    def write(self):
        with open(self.name, "w") as f:
            f.write(json.dumps(self.data, separators=(",", ":")))

    def get(self, key, default=None):
        return self.data.get(key, default)

    def set(self, key, value):
        print(f"Setting {key} to {value}")
        self.data[key] = value
        self.write()


config = Storage("config")
