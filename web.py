import re
import time

from storage import config
from tracker import Tracker


def unquote_plus(string):
    string = string.replace("+", " ")
    arr = string.split("%")
    arr2 = [chr(int(part[:2], 16)) + part[2:] for part in arr[1:]]
    return arr[0] + "".join(arr2)


def tr(t):
    return f"<tr>{t}</tr>"


def td(t):
    return f"<td>{t}</td>"


def format_time(t: int):
    return "{0:04d}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}".format(*time.localtime(t))


def handle_request(client_socket):
    request = client_socket.recv(1024)
    request = request.decode("utf-8")
    if "favicon.ico" in request:
        client_socket.close()
        return

    switch_pattern = re.compile(r"GET /\?switch=([01])")
    mac_pattern = re.compile(r"GET /\?mac=([a-fA-F0-9,%]+)")

    match = switch_pattern.match(request)
    if match:
        config.set("locked", bool(int(match.group(1))))

        client_socket.send("HTTP/1.1 307 Temporary Redirect\r\nLocation: /")
        client_socket.close()
        return

    match = mac_pattern.match(request)
    if match:
        trackers = unquote_plus(match.group(1)).split(",")
        config.set("trackers", trackers)
        Tracker.init(trackers)

        client_socket.send("HTTP/1.1 307 Temporary Redirect\r\nLocation: /")
        client_socket.close()
        return

    with open("index.html", "b") as f:
        trackers = [
            td(i.mac) + td(format_time(i.last_seen)) + td(i.rssi)
            for i in Tracker.tracking.values()
        ]
        header = b"HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n"
        payload = {
            "locked": "checked" if config.get("locked") else "",
            "trackers": "".join((tr(i) for i in trackers)),
            "mac": ",".join(config.get("trackers", [])),
        }
        client_socket.send(header + f.read() % payload)

    client_socket.close()
