import time

try:
    from machine import Pin
except ImportError:
    class Pin:
        OUT = object()
        PULL_DOWN = object()

        def __init__(self, *args, **kwargs):
            pass

        def on(self):
            print(f"{id(self)} on")

        def off(self):
            print(f"{id(self)} off")


class Action:
    OPEN = "open"
    CLOSE = "close"
    OFF = "off"


class Door:
    action: str
    state: str = ""
    start: float

    def __init__(self, ba: int, bb: int, delay: int = 5):
        self.ba = Pin(ba, mode=Pin.OUT, value=0, pull=Pin.PULL_DOWN)
        self.bb = Pin(bb, mode=Pin.OUT, value=0, pull=Pin.PULL_DOWN)
        self.delay = delay
        self.off()

    def open(self):
        if self.state == Action.OPEN:
            self.work()
            return
        if self.action == Action.OPEN:
            self.work()
            return
        self.state = None
        self.ba.on()
        self.bb.off()
        self.start = time.time()
        self.action = Action.OPEN
        print(f"Door action {self.action}")

    def close(self):
        if self.state == Action.CLOSE:
            self.work()
            return
        if self.action == Action.CLOSE:
            self.work()
            return
        self.state = None
        self.ba.off()
        self.bb.on()
        self.start = time.time()
        self.action = Action.CLOSE
        print(f"Door action {self.action}")

    def off(self):
        self.ba.off()
        self.bb.off()
        self.action = Action.OFF
        print(f"Door action {self.action}")

    def work(self):
        if self.action != Action.OFF and self.start + self.delay < time.time():
            self.state = self.action
            self.off()
            print(f"Door state {self.state}")


if __name__ == "__main__":
    d = Door(27, 26)

    print("Open")
    for _ in range(10):
        d.open()
        time.sleep(1)

    print("Close")
    for _ in range(10):
        d.close()
        time.sleep(1)
