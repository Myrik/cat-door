import time

TRESHOLD = -80


class Tracker:
    tracking = {}

    def __init__(self, mac):
        self.mac = mac
        self.last_seen = 0
        self.rssi = -1000

    def track(self, rssi):
        self.rssi = rssi
        if TRESHOLD < rssi:
            self.last_seen = time.time()

    def __eq__(self, other):
        if isinstance(other, str):
            return self.mac == other
        elif isinstance(other, Tracker):
            return self.mac == other.mac
        return False

    @classmethod
    def init(cls, trackers: list[str]):
        cls.tracking = {i: cls(i) for i in trackers}
